import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import play.api.libs.json._

import scala.util.Try


object SimpleMain {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
    conf.setAppName("BigData-AlexandreKahn")
    conf.setMaster("local[8]") // CHANGE THIS
    val sc = new SparkContext(conf)


    type Tag = String
    type Likes = Int

    case class User(var followers: Int,
                    var friends: Int)

    case class Tweet(text: String,
                     user: User,
                     hashTags: Array[Tag],
                     likes: Likes)

    def parseTweet(tweet: String):  Option[Some[Tweet]] = {
      Try {
        val json: JsValue = Json.parse(tweet)

        var text: String = ""
        val user: User = User(0, 0)
        var hashTags: Array[Tag] = new Array[Tag](1)
        var likes: Int = 0
        var followers: Int = 0
        var friends: Int = 0

        val retweeted = (json \ "retweeted_status").asOpt[JsValue]
        val quoted = (json \ "quoted_status").asOpt[JsValue]
//        println(quoted)
//        println(retweeted)
        if (quoted != None) {

          text = (json \ "quoted_status" \ "text").as[String]
          followers = (json \ "quoted_status" \ "user" \ "followers_count").as[Int]
          friends = (json \ "quoted_status" \ "user" \ "friends_count").as[Int]
          hashTags = (json \ "quoted_status" \ "entities" \ "hashtags").as[Array[JsValue]]
            .map(tag => (tag \ "text").as[String])
          likes = (json \ "quoted_status" \ "favourites_count").as[Int]

        }
        else if (retweeted != None) {
          text = (json \ "retweeted_status" \ "text").as[String]
          followers = (json \ "retweeted_status" \ "user" \ "followers_count").as[Int]
          friends = (json \ "retweeted_status" \ "user" \ "friends_count").as[Int]
          hashTags = (json \ "retweeted_status" \ "entities" \ "hashtags").as[Array[JsValue]]
            .map(tag => (tag \ "text").as[String])
          likes = (json \ "retweeted_status" \ "favorite_count").as[Int]
//          println(likes)
        }


        user.followers = followers
        user.friends = friends

//        println(Tweet(text, user, hashTags, likes))

        Some(Tweet(text, user, hashTags, likes))
      }.toOption
    }

    var pre = sc.textFile("data/twitter/tweets.txt")
      .flatMap(parseTweet)
//    pre.collect().foreach(println)
    val tweets = pre.map { case Some(tweet) => tweet}
      .filter(twt => twt.text != "")


    type Feature = Double
    type Followers = Feature
    type Friends = Feature
    type NumericalTag = Feature
    type NumberOfTags = Feature
    type FeatureTuple = (Feature, Followers, Friends, NumericalTag, NumberOfTags, Likes)
    // Followers, Following, Frequency of hashtag, Number of tags

    def extractFeatures(tweets: RDD[Tweet]): RDD[FeatureTuple] = {

      // First search the frequency of each hashtag
      // Put it in a Dict -> map each hashtag to it frequency
      // Higher frequency == higher score

      // Loosely based on https://stackoverflow.com/questions/31820187/how-to-calculate-term-frequency-for-a-document-in-spark
      val tags: RDD[(Tag, Int)] = tweets.flatMap(tweet => (tweet.hashTags.map(tag => (tag, 1))))
      val tagFrequency = tags.reduceByKey(_ + _).collectAsMap()

      tweets.flatMap(tweet => (tweet.hashTags.map(tag => (1.toDouble,
        tweet.user.followers.toDouble,
        tweet.user.friends.toDouble,
        tagFrequency.getOrElse(tag, 0).toDouble,
        tweet.hashTags.length.toDouble,
        tweet.likes))))
    }

    val featureRDD = extractFeatures(tweets)
    val length = featureRDD.count()


    def scaleFeatures(featureRDD: RDD[FeatureTuple]): RDD[FeatureTuple] = {
      val sumx0 = featureRDD.map(tweettuple => tweettuple._1).fold(0)((acc, s) => acc + s)
      val sumx1 = featureRDD.map(t => t._2).fold(0)((acc, s) => acc + s)
      val sumx2 = featureRDD.map(t => t._3).fold(0)((acc, s) => acc + s)
      val sumx3 = featureRDD.map(t => t._4).fold(0)((acc, s) => acc + s)
      val sumx4 = featureRDD.map(t => t._5).fold(0)((acc, s) => acc + s)
      // I think the y value has not to be rescaled


      val avgtuple = (sumx0 / length, sumx1 / length, sumx2 / length, sumx3 / length, sumx4 / length)

      val stdev0 = math.sqrt((featureRDD.map(t => t._1).fold(0)((acc, s) => (acc + Math.pow((s - avgtuple._1), 2)))) / (length - 1))
      val stdev1 = math.sqrt((featureRDD.map(t => t._2).fold(0)((acc, s) => (acc + Math.pow((s - avgtuple._2), 2)))) / (length - 1))
      val stdev2 = math.sqrt((featureRDD.map(t => t._3).fold(0)((acc, s) => (acc + Math.pow((s - avgtuple._3), 2)))) / (length - 1))
      val stdev3 = math.sqrt((featureRDD.map(t => t._4).fold(0)((acc, s) => (acc + Math.pow((s - avgtuple._4), 2)))) / (length - 1))
      val stdev4 = math.sqrt((featureRDD.map(t => t._5).fold(0)((acc, s) => (acc + Math.pow((s - avgtuple._5), 2)))) / (length - 1))

      featureRDD.map(tweet => (((tweet._1 - avgtuple._1) / stdev0), ((tweet._2 - avgtuple._2) / stdev1), ((tweet._3 - avgtuple._3) / stdev2), ((tweet._4 - avgtuple._4) / stdev3), ((tweet._5 - avgtuple._5) / stdev4), tweet._6))
    }

    val scaledFeatureRDD = scaleFeatures(featureRDD)

    tweets.collect().foreach(println)
    featureRDD.collect().foreach(println)
    scaledFeatureRDD.collect().foreach(println)


    type Theta = Array[Double]

    def hThetaTimesXMinusYCost(featureTuple: RDD[FeatureTuple], theta: Theta): RDD[(Double)] = {
      scaledFeatureRDD.map(tweet => ((tweet._1 * theta(0)) + (tweet._2 * theta(1)) + (tweet._3 * theta(2)) + (tweet._4 * theta(3)) + (tweet._5 * theta(4)) - tweet._6))
    }

    def hThetaTimesXMinusY(theta: Theta, featureTuple: RDD[FeatureTuple]): RDD[(FeatureTuple)] = {

      def fct(tweet: FeatureTuple): Double = (((tweet._1 * theta(0)) + (tweet._2 * theta(1)) + (tweet._3 * theta(2)) + (tweet._4 * theta(3)) + (tweet._5 * theta(4))) - tweet._6)

      scaledFeatureRDD.map(tweet => (fct(tweet) * tweet._1, fct(tweet) * tweet._2, fct(tweet) * tweet._3, fct(tweet) * tweet._4, fct(tweet) * tweet._5, tweet._6))
    }


    def cost(scaledFeatureRDD: RDD[FeatureTuple], theta: Theta): Double = {
      val hTheta: RDD[Double] = hThetaTimesXMinusYCost(scaledFeatureRDD, theta)
      hTheta.fold(0)((acc, s) => (acc + math.pow(s, 2))) / (2 * length)
    }

    //    def summation(idx: Int, scaledFeatureRDD: RDD[FeatureTuple], theta: Theta): Double = {
    //      val hTheta: RDD[(Double, FeatureTuple)] = hThetaTimesXMinusY(scaledFeatureRDD, theta)
    //      hTheta.map(.fold(0)((acc, s) =>)
    //
    //    }


    def gradientDecent(scaledFeatureRDD: RDD[FeatureTuple],
                       theta: Theta,
                       alpha: Double,
                       sigma: Double): Theta = {
      var error: Double = cost(scaledFeatureRDD, theta)
      var delta: Double = 0
      do {

        val tuple: RDD[FeatureTuple] = hThetaTimesXMinusY(theta, featureRDD)
        theta(0) = (theta(0) - ((alpha / length) * tuple.map(t => t._1).fold(0)((acc, s) => acc + s)))
        theta(1) = (theta(1) - ((alpha / length) * tuple.map(t => t._2).fold(0)((acc, s) => acc + s)))
        theta(2) = (theta(2) - ((alpha / length) * tuple.map(t => t._3).fold(0)((acc, s) => acc + s)))
        theta(3) = (theta(3) - ((alpha / length) * tuple.map(t => t._4).fold(0)((acc, s) => acc + s)))
        theta(4) = (theta(2) - ((alpha / length) * tuple.map(t => t._5).fold(0)((acc, s) => acc + s)))


        val newJTheta: Double = cost(scaledFeatureRDD, theta)
        delta = error - newJTheta // ALbeit new theta
        error = newJTheta
      }
      while (delta > sigma)
      return theta
    }

    gradientDecent(scaledFeatureRDD, Array(0, 0, 0, 0, 0), 0.001, 0.001)

  }

}