name := "ProjectBigData"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.4.0"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.4.11"


libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.7.1"
//scalacOptions ++= Seq("-verbose", "-Ylog-classpath")
